package com.example.joinstore.esercitazionefrancesco;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class myAdapter extends RecyclerView.Adapter<myAdapter.ViewHolder> {

    private final int nData;
    Context cx;

    public myAdapter(Context cx, int nData) {
        this.nData = nData;
        this.cx = cx;
    }

    @Override
    public myAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.data_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String pos = Integer.toString(position);
        holder.text.setText(pos);
        holder.button.setText("Premi");
        holder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast toast = Toast.makeText(cx, pos, Toast.LENGTH_SHORT);
                toast.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.nData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View view;
        public final TextView text;
        public final Button button;


        public ViewHolder(View v) {
            super(v);
            view = v;
            text = view.findViewById(R.id.simple_text);
            button = view.findViewById(R.id.simple_button);


        }
    }
}
