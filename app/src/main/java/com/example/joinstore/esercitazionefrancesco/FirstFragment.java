package com.example.joinstore.esercitazionefrancesco;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FirstFragment extends Fragment {

    public FirstFragment(){}

    public static FirstFragment newInstance() {
        FirstFragment frag = new FirstFragment();
        return frag;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_first, container, false);
        TextView message = view.findViewById(R.id.mex);
        if(getArguments() != null) {
            message.setText(getArguments().getString("Message"));
        }

        RecyclerView mRecyclerView = view.findViewById(R.id.fragment_recycler);
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        // specify an adapter (see also next example)
        RecyclerView.Adapter mAdapter = new myAdapter(getActivity(), 20);
        mRecyclerView.setAdapter(mAdapter);
        return view;
    }
}
