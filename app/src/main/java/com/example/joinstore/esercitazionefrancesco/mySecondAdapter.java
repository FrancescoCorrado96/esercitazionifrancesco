package com.example.joinstore.esercitazionefrancesco;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class mySecondAdapter extends RecyclerView.Adapter<mySecondAdapter.ViewHolder> {

    private final int nData;
    private Context cx;

    public mySecondAdapter(Context context, int nData) {
        this.nData = nData;
        this.cx = context;
    }

    @Override
    public mySecondAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.second_data_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final String pos = Integer.toString(position);
        holder.button1.setText("Premi");
        holder.button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(cx)
                        .setMessage(pos)
                        .setTitle("Posizione")
                        .create()
                        .show();
            }
        });
        holder.button2.setText("Premi più forte");
    }

    @Override
    public int getItemCount() {
        return this.nData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View view;
        public final Button button1;
        public final Button button2;


        public ViewHolder(View v) {
            super(v);
            view = v;
            button1 = view.findViewById(R.id.second_fragment_button_1);
            button2 = view.findViewById(R.id.second_fragment_button_2);
        }
    }
}
